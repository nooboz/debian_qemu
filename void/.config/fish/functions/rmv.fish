function rmv --wraps='sudo xbps-remove -ROo' --description 'alias rmv sudo xbps-remove -ROo'
  sudo xbps-remove -ROo $argv
        
end
