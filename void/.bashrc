# .bashrc

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias grep='grep --color=auto'
# PS1='[\u@\h \W]\$ '
# PS1='[\h \w]\$ '
####PS1='[\h \[\e[1;34m\]\w\[\e[0m\]]\$ '
#PS1='[\h \[\e[1;34m\]\w\[\e[0m\]]\$ '
PS1='[\[\e[1;32m\]\w\[\e[0m\]]\n\[\e[1;32m\]\$\[\e[0m\] '


alias a=neofetch
# alias sx=startx
alias sx=startxfce4
alias b='chromium -incognito'
alias v=vim
alias r='loginctl reboot'
alias p='loginctl poweroff'
alias e='exit 0'
alias f='free -h'
alias tor='tor-browser/Browser/start-tor-browser'
alias u='sudo xbps-install -Su'
alias ux='sudo xbps-install -u xbps'
alias i='sudo xbps-install -S'
alias c='sudo vkpurge rm all'
alias rmv='sudo xbps-remove -ROo'
alias d='curl -L -O -C -'
alias s='xbps-query -Rs'
#alias t='setterm --blank 1'
alias ph='pd -font-face "Fira Code Medium" -font-size 12 -font-weight bold'
alias pb='pd -nrt -font-face "Fira" -font-size 12 -font-weight normal'

bind 'TAB:menu-complete'
#bind 'set show-all-if-ambiguous on'
#bind 'set completion-ignore-case on'
#bind '"\e[Z":menu-complete-backward'
#bind '"\e[A":history-search-backward'
#bind '"\e[B":history-search-forward'

HISTTIMEFORMAT="%Y-%m-%d %T "
#HISTSIZE=10998000
##HISTFILESIZE=10998000
shopt -s histappend
#
HISTSIZE= HISTFILESIZE= # Infinite history.
shopt -s autocd #Allows you to cd into directory merely by typing the directory name.

shopt -s cdspell

#!/bin/sh
if [ "$TERM" = "linux" ]; then
  /bin/echo -e "
  \e]P0151515
  \e]P1ac4142
  \e]P290a959
  \e]P3f4bf75
  \e]P46a9fb5
  \e]P5aa759f
  \e]P675b5aa
  \e]P7d0d0d0
  \e]P8505050
  \e]P9ac4142
  \e]PA90a959
  \e]PBf4bf75
  \e]PC6a9fb5
  \e]PDaa759f
  \e]PE75b5aa
  \e]PFf5f5f5
  "
  # get rid of artifacts
  clear
fi
####

export VISUAL="vim"
export EDITOR="$VISUAL"
# export XCURSOR_SIZE=16
# export TERMINAL="kitty"
# export TERMINAL="mate-terminal"
export TERMINAL="xfce4-terminal"
export BROWSER="chromium"

# alias b='chromium -incognito'
alias cb='v ~/.bashrc'
alias k='uname -r'
