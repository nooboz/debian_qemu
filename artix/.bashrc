#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# alias ls='ls --color=auto'
# alias grep='grep --color=auto'
# PS1='[\u@\h \W]\$ '
# PS1='[\w]\$ '

alias b='firefox --private-window'

alias sx=startx

alias a=neofetch
