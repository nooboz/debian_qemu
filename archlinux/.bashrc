#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

#alias ls='ls --color=auto'
#alias grep='grep --color=auto'
# PS1='[\u@\h \W]\$ '

##############################
alias ls='ls --color=auto'
#alias dir='dir --color=auto'
#alias vdir='vdir --color=auto'
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'
HISTTIMEFORMAT="%Y-%m-%d %T "
# PS1='[\[\e[1;34m\]\w\[\e[0m\]]\[\e[1;34m\]\$\[\e[0m\] '
# PS1='[\[\e[1;34m\]\w\[\e[0m\]]\D{%A-%d-%B(%m) %Y - %H:%M:%S}\[\e[1;34m\]\$\[\e[0m\] '
PS1='[\[\e[1;34m\]\w\[\e[0m\]] \D{%A %d %B(%m) %Y - %H:%M:%S}\n\[\e[1;34m\]\$\[\e[0m\] '

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

shopt -s histappend
shopt -s checkwinsize
shopt -s cdspell
shopt -s autocd #Allows you to cd directory mrely by typing the durtory name.
HISTSIZE= HISTFILESIZE= # Infinite history.

alias grep='grep --color=auto'
alias a=neofetch
alias e='exit 0'
# alias b='chromium --incognito'
alias sx=startx
alias t='top -iE g'
alias t2='top -i1E g'
alias p='systemctl poweroff'
alias d='curl -L -O -C -'
alias tor='~/tor-browser/Browser/start-tor-browser'
alias u='sudo pacman -Syu'
alias ux='sudo pacman -Syyu'
alias r='systemctl reboot'
alias c='sudo pacman -Sc'
alias s='pacman -Ss'
alias dm='youtube-dl --prefer-ffmpeg --extract-audio --audio-format mp3 --audio-quality 0 --embed-thumbnail'
alias dv='youtube-dl --add-metadata -ic'
# alias grep='grep --color=always'
#alias grep='grep --color=auto'
alias dir='dir --color=auto'
alias vdir='vdir --color=auto'
alias z='screen urxvt -e sh'
#alias pd='pd -font-face "Hack" -font-size 12 -font-weight normal'
#alias pd='pd -font-face "Fira Code" -font-size 12 -font-weight bold'
alias pd='puredata -font-face "JetBrains Mono" -font-size 12 -font-weight bold'
alias pdn='pd -nrt -font-face "Hack" -font-size 12 -font-weight normal'
# alias b='brave --password-store=basic --incognito %U'
alias b='firefox --private-window'
alias v=vim
alias luna='curl -Ss 'https://wttr.in/moon''
# No history
alias b2="bash --init-file <(echo '. ~/.bashrc; unset HISTFILE')"
# or:    And better put it in ~/.bash_aliases as permanent alias:
#alias b3='bash --rcfile <(echo '. ~/.bashrc; unset HISTFILE')'
alias m='brave --incognito https://www.3bmeteo.com/meteo/bologna'
alias uptime='uptime -sp && uptime'

#source ~/.bashrc
##########################################################################
#!/bin/sh
if [ "$TERM" = "linux" ]; then
  /bin/echo -e "
  \e]P0151515
  \e]P1ac4142
  \e]P290a959
  \e]P3f4bf75
  \e]P46a9fb5
  \e]P5aa759f
  \e]P675b5aa
  \e]P7d0d0d0
  \e]P8505050
  \e]P9ac4142
  \e]PA90a959
  \e]PBf4bf75
  \e]PC6a9fb5
  \e]PDaa759f
  \e]PE75b5aa
  \e]PFf5f5f5
  "
  # get rid of artifacts
  clear
fi
###########################################################################

export VISUAL="vim"
export EDITOR="$VISUAL"
###########################################################################
export TERMINAL="alacritty"
export BROWSER="brave"
####
export MYVIMRC="$HOME/.vimrc"
export VIMINIT="source $MYVIMRC"

export LC_ALL=it_IT.UTF-8

alias al='alsamixer -c 1'

alias cb='v ~/.bashrc'

# [[ -z $DISPLAY && $XDG_VTNR -eq 1 ]] && exec startx


