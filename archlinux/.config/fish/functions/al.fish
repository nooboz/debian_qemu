function al --wraps='alsamixer -c 1' --description 'alias al alsamixer -c 1'
  alsamixer -c 1 $argv
        
end
