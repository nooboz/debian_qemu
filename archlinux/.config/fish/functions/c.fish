function c --wraps='chromium --incognito' --description 'alias c chromium --incognito'
  chromium --incognito $argv
        
end
