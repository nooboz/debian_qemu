function tor --wraps=tor-browser/Browser/start-tor-browser --description 'alias tor tor-browser/Browser/start-tor-browser'
  tor-browser/Browser/start-tor-browser $argv
        
end
