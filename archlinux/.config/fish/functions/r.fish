function r --wraps='systemctl reboot' --description 'alias r systemctl reboot'
  systemctl reboot $argv
        
end
