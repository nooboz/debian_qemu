function p --wraps='systemctl poweroff' --description 'alias p systemctl poweroff'
  systemctl poweroff $argv
        
end
